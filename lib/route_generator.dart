import 'package:deliveryboy/src/pages/drivers_map.dart';
import 'package:deliveryboy/src/pages/map.dart';
import 'package:deliveryboy/src/pages/notifications_resturant.dart';
import 'package:deliveryboy/src/pages/pages-admin.dart';
import 'package:deliveryboy/src/pages/resturant_all_orders.dart';
import 'package:deliveryboy/src/pages/resturant_foods.dart';
import 'package:flutter/material.dart';

import 'src/models/route_argument.dart';
import 'src/pages/forget_password.dart';
import 'src/pages/help.dart';
import 'src/pages/languages.dart';
import 'src/pages/login.dart';
import 'src/pages/notifications.dart';
import 'src/pages/order.dart';
import 'src/pages/pages-resturant.dart';
import 'src/pages/pages.dart';
import 'src/pages/resturant_order.dart';
import 'src/pages/settings.dart';
import 'src/pages/signup.dart';
import 'src/pages/splash_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/SignUp':
        return MaterialPageRoute(builder: (_) => SignUpWidget());
      case '/MobileVerification':
        return MaterialPageRoute(builder: (_) => SignUpWidget());
      case '/MobileVerification2':
        return MaterialPageRoute(builder: (_) => SignUpWidget());
      case '/Login':
        return MaterialPageRoute(builder: (_) => LoginWidget());
      case '/ForgetPassword':
        return MaterialPageRoute(builder: (_) => ForgetPasswordWidget());
      case '/Pages':
        return MaterialPageRoute(
            builder: (_) => PagesTestWidget(currentTab: args));
      case '/OrderDetails':
        return MaterialPageRoute(
            builder: (_) => OrderWidget(routeArgument: args as RouteArgument));
      case '/ResturantOrderDetails':
        return MaterialPageRoute(
            builder: (_) =>
                RestaurantOrderWidget(routeArgument: args as RouteArgument));
      case '/Notifications':
        return MaterialPageRoute(builder: (_) => NotificationsWidget());
      case '/NotificationsRestaurant':
        return MaterialPageRoute(
            builder: (_) => NotificationsRestaurantWidget());
      case '/Languages':
        return MaterialPageRoute(builder: (_) => LanguagesWidget());
      case '/Help':
        return MaterialPageRoute(builder: (_) => HelpWidget());
      case '/Settings':
        return MaterialPageRoute(builder: (_) => SettingsWidget());
      case '/PagesRestaurant':
        return MaterialPageRoute(
            builder: (_) => PagesRestaurantWidget(currentTab: args));
      case '/Map':
        return MaterialPageRoute(
            builder: (_) => MapWidget(routeArgument: args as RouteArgument));
      case '/ResturantAllOrders':
        return MaterialPageRoute(builder: (_) => ResturantAllOrdersWidget());
      case '/DriversMap':
        return MaterialPageRoute(
            builder: (_) =>
                DriversMapWidget(routeArgument: args as RouteArgument));
      case '/PagesAdmin':
        return MaterialPageRoute(
            builder: (_) => PagesAdminWidget(currentTab: args));
      case '/RestuarantFoods':
        return MaterialPageRoute(
            builder: (_) =>
                RestuarantFoodsWidget(routeArgument: args as RouteArgument));
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return MaterialPageRoute(
            builder: (_) =>
                Scaffold(body: SafeArea(child: Text('Route Error'))));
    }
  }
}
