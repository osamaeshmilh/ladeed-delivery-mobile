import 'package:audioplayers/audio_cache.dart';

class Fcm {
  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) {
    AudioCache player = new AudioCache();
    const alarmAudioPath = "sound/swiftly.mp3";
    player.play(alarmAudioPath);
    return null;
  }
}
