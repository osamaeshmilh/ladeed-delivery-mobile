import 'package:deliveryboy/src/helpers/helper.dart';
import 'package:deliveryboy/src/models/restaurant.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/order.dart';
import '../models/user.dart';
import '../repository/order_repository.dart';
import '../repository/user_repository.dart';

class ProfileController extends ControllerMVC {
  User user = new User();
  Restaurant restaurant = new Restaurant();
  List<Order> recentOrders = [];
  GlobalKey<ScaffoldState> scaffoldKey;
  OverlayEntry loader;

  ProfileController() {
    loader = Helper.overlayLoader(context);
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    listenForUser();
  }

  void listenForUser() {
    getCurrentUser().then((_user) {
      setState(() {
        user = _user;
      });
    });
  }

  void listenForUserRestaurant() {
    //Overlay.of(context).insert(loader);
    getCurrentRestaurant().then((_restaurant) {
      setState(() {
        restaurant = _restaurant;
      });
    });
  }

  void listenUpdateRestaurant() {
    Overlay.of(context).insert(loader);
    updateRestaurant(restaurant).then((_restaurant) {
      loader.remove();
      listenForUserRestaurant();
    }).catchError((onError) {
      loader.remove();
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text('حدث خطأ ما !'),
      ));
    });
  }

  void listenForRecentOrders({String message}) async {
    final Stream<Order> stream = await getRecentOrders();
    stream.listen((Order _order) {
      setState(() {
        recentOrders.add(_order);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshProfile() async {
    recentOrders.clear();
    user = new User();
    listenForRecentOrders(message: S.of(context).orders_refreshed_successfuly);
    listenForUser();
  }

  Future<void> refreshRestaurantProfile() async {
    restaurant = new Restaurant();
    listenForUserRestaurant();
    listenUpdateRestaurant();
  }
}
