import 'dart:async';

import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';
import '../helpers/maps_util.dart';
import '../models/address.dart';
import '../models/order.dart';
import '../repository/order_repository.dart';
import '../repository/settings_repository.dart' as sett;

class MapController extends ControllerMVC {
  Order currentOrder;
  List<Order> orders = <Order>[];
  List<Marker> allMarkers = <Marker>[];
  Address currentAddress;
  Set<Polyline> polylines = new Set();
  CameraPosition cameraPosition;
  MapsUtil mapsUtil = new MapsUtil();
  Completer<GoogleMapController> mapController = Completer();

  void listenForNearOrders(Address myAddress, Address areaAddress) async {
    print('listenForOrders');
    final Stream<Order> stream = await getNearOrders(myAddress, areaAddress);
    stream.listen(
        (Order _order) {
          setState(() {
            orders.add(_order);
          });
          if (!_order.deliveryAddress.isUnknown()) {
            Helper.getOrderMarker(_order.deliveryAddress.toMap())
                .then((marker) {
              setState(() {
                allMarkers.add(marker);
              });
            });
          }
        },
        onError: (a) {},
        onDone: () {
        });
  }

  void getCurrentLocation() async {
    var location = new Location();
    location.onLocationChanged.listen((LocationData current) {
      try {
        setState(() {
          cameraPosition = CameraPosition(
            target: LatLng(current.latitude, current.longitude),
            zoom: 14.4746,
          );
        });
//          Helper.getMyPositionMarker(
//              current.latitude, current.longitude)
//              .then((marker) {
//            setState(() {
//              allMarkers.add(marker);
//            });
//          });
      } on PlatformException catch (e) {
        if (e.code == 'PERMISSION_DENIED') {
          print('Permission denied');
        }
      }
    });
  }

  void getOrderLocation() async {
    try {
      setState(() {
        cameraPosition = CameraPosition(
          target: LatLng(currentOrder.deliveryAddress.latitude,
              currentOrder.deliveryAddress.longitude),
          zoom: 14.4746,
        );
      });
      var location = new Location();
      location.onLocationChanged.listen((LocationData current) {
//          Helper.getMyPositionMarker(
//              current.latitude, current.longitude)
//              .then((marker) {
//            setState(() {
//              allMarkers.add(marker);
//            });
//          });
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      }
    }
  }

  Future<void> goCurrentLocation() async {
    final GoogleMapController controller = await mapController.future;

    sett.setCurrentLocation().then((_currentAddress) {
      setState(() {
        sett.myAddress.value = _currentAddress;
        currentAddress = _currentAddress;
      });
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(_currentAddress.latitude, _currentAddress.longitude),
        zoom: 14.4746,
      )));
    });
  }

  void getOrdersOfArea() async {
    setState(() {
      orders = <Order>[];
      Address areaAddress = Address.fromJSON({
        "latitude": cameraPosition.target.latitude,
        "longitude": cameraPosition.target.longitude
      });
      if (cameraPosition != null) {
        listenForNearOrders(currentAddress, areaAddress);
      } else {
        listenForNearOrders(currentAddress, currentAddress);
      }
    });
  }

  void getDirectionSteps() async {
    var location = new Location();
    location.onLocationChanged.listen((LocationData current) {
      mapsUtil
          .get("origin=" +
              current.latitude.toString() +
              "," +
              current.longitude.toString() +
              "&destination=" +
              currentOrder.deliveryAddress.latitude.toString() +
              "," +
              currentOrder.deliveryAddress.longitude.toString() +
              "&key=${sett.setting.value?.googleMapsKey}")
          .then((dynamic res) {
        if (res != null) {
          List<LatLng> _latLng = res as List<LatLng>;
          _latLng?.insert(0, new LatLng(current.latitude, current.longitude));
          setState(() {
            polylines.add(new Polyline(
                visible: true,
                polylineId: new PolylineId(currentAddress.hashCode.toString()),
                points: _latLng,
                color: config.Colors().mainColor(0.8),
                width: 6));
          });
        }
      });
    });
  }

  Future refreshMap() async {
    setState(() {
      orders = <Order>[];
    });
    listenForNearOrders(currentAddress, currentAddress);
  }
}
