import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/order.dart';
import '../repository/order_repository.dart';

class OrderController extends ControllerMVC {
  List<Order> orders = <Order>[];
  List<Order> ordersWaiting = <Order>[];
  List<Order> ordersMaking = <Order>[];
  List<Order> ordersReady = <Order>[];
  List<Order> ordersOut = <Order>[];
  List<Order> ordersDelivered = <Order>[];
  List<Order> ordersCancelled = <Order>[];

  GlobalKey<ScaffoldState> scaffoldKey;

  OrderController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void listenForOrders({String message}) async {
    final Stream<Order> stream = await getOrders();
    stream.listen((Order _order) {
      setState(() {
        orders.add(_order);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void listenForOrdersHistory({String message}) async {
    final Stream<Order> stream = await getOrdersHistory();
    stream.listen((Order _order) {
      setState(() {
        orders.add(_order);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshOrdersHistory() async {
    orders.clear();
    listenForOrdersHistory(message: S.of(context).order_refreshed_successfuly);
  }

  Future<void> refreshOrders() async {
    orders.clear();
    listenForOrders(message: S.of(context).order_refreshed_successfuly);
  }

  void listenForRestaurantOrders({String message}) async {
    final Stream<Order> stream = await getRestaurantOrders();
    stream.listen((Order _order) {
      setState(() {
        if (_order.orderStatus.id == '1' && _order.active == true)
          ordersWaiting.add(_order);
        if (_order.orderStatus.id == '2' && _order.active == true)
          ordersMaking.add(_order);
        if (_order.orderStatus.id == '3' && _order.active == true)
          ordersReady.add(_order);
        if (_order.orderStatus.id == '4' && _order.active == true)
          ordersOut.add(_order);
        if (_order.orderStatus.id == '5' && _order.active == true)
          ordersDelivered.add(_order);
        if (_order.active == false) ordersCancelled.add(_order);
        orders.add(_order);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshRestaurantOrders() async {
    orders.clear();
    ordersWaiting.clear();
    ordersMaking.clear();
    ordersReady.clear();
    ordersOut.clear();
    ordersDelivered.clear();
    ordersCancelled.clear();
    listenForRestaurantOrders(
        message: S.of(context).order_refreshed_successfuly);
  }

  void listenForRestaurantAllOrders({String message}) async {
    final Stream<Order> stream = await getRestaurantAllOrders();
    stream.listen((Order _order) {
      setState(() {
        if (_order.orderStatus.id == '1' && _order.active == true)
          ordersWaiting.add(_order);
        if (_order.orderStatus.id == '2' && _order.active == true)
          ordersMaking.add(_order);
        if (_order.orderStatus.id == '3' && _order.active == true)
          ordersReady.add(_order);
        if (_order.orderStatus.id == '4' && _order.active == true)
          ordersOut.add(_order);
        if (_order.orderStatus.id == '5' && _order.active == true)
          ordersDelivered.add(_order);
        if (_order.active == false) ordersCancelled.add(_order);
        orders.add(_order);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshRestaurantAllOrders() async {
    orders.clear();
    ordersWaiting.clear();
    ordersMaking.clear();
    ordersReady.clear();
    ordersOut.clear();
    ordersDelivered.clear();
    ordersCancelled.clear();
    listenForRestaurantAllOrders(
        message: S.of(context).order_refreshed_successfuly);
  }

  void listenForAdminOrders({String message}) async {
    final Stream<Order> stream = await getAdminOrders();
    stream.listen((Order _order) {
      setState(() {
        if (_order.orderStatus.id == '1' && _order.active == true)
          ordersWaiting.add(_order);
        if (_order.orderStatus.id == '2' && _order.active == true)
          ordersMaking.add(_order);
        if (_order.orderStatus.id == '3' && _order.active == true)
          ordersReady.add(_order);
        if (_order.orderStatus.id == '4' && _order.active == true)
          ordersOut.add(_order);
        if (_order.orderStatus.id == '5' && _order.active == true)
          ordersDelivered.add(_order);
        if (_order.active == false) ordersCancelled.add(_order);
        orders.add(_order);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshAdminOrders() async {
    orders.clear();
    ordersWaiting.clear();
    ordersMaking.clear();
    ordersReady.clear();
    ordersOut.clear();
    ordersDelivered.clear();
    ordersCancelled.clear();
    listenForAdminOrders(message: S
        .of(context)
        .order_refreshed_successfuly);
  }

  void listenForAdminAllOrders({String message}) async {
    final Stream<Order> stream = await getAdminAllOrders();
    stream.listen((Order _order) {
      setState(() {
        if (_order.orderStatus.id == '1' && _order.active == true)
          ordersWaiting.add(_order);
        if (_order.orderStatus.id == '2' && _order.active == true)
          ordersMaking.add(_order);
        if (_order.orderStatus.id == '3' && _order.active == true)
          ordersReady.add(_order);
        if (_order.orderStatus.id == '4' && _order.active == true)
          ordersOut.add(_order);
        if (_order.orderStatus.id == '5' && _order.active == true)
          ordersDelivered.add(_order);
        if (_order.active == false) ordersCancelled.add(_order);
        orders.add(_order);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S
            .of(context)
            .verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshAdminAllOrders() async {
    orders.clear();
    ordersWaiting.clear();
    ordersMaking.clear();
    ordersReady.clear();
    ordersOut.clear();
    ordersDelivered.clear();
    ordersCancelled.clear();
    listenForAdminAllOrders(message: S
        .of(context)
        .order_refreshed_successfuly);
  }
}
