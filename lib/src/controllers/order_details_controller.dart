import 'package:deliveryboy/src/models/order_status.dart';
import 'package:deliveryboy/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/order.dart';
import '../repository/order_repository.dart';

class OrderDetailsController extends ControllerMVC {
  Order order;
  GlobalKey<ScaffoldState> scaffoldKey;
  List<OrderStatus> orderStatus = <OrderStatus>[];
  List<User> drivers = <User>[];
  TextEditingController hintText = new TextEditingController();

  OrderDetailsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void listenForOrder({String id, String message}) async {
    final Stream<Order> stream = await getOrder(id);
    stream.listen((Order _order) {
      setState(() => order = _order);
    }, onError: (a) {
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshOrder() async {
    listenForOrder(
        id: order.id, message: S.of(context).order_refreshed_successfuly);
  }

  void doDeliveredOrder(Order _order) async {
    deliveredOrder(_order).then((value) {
      setState(() {
        this.order.orderStatus.id = '5';
      });
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text('تم توصيل الطلب الى الزبون !'),
      ));
    });
  }

  void listenForOrderStatus() async {
    final Stream<OrderStatus> stream = await getOrderStatus();
    stream.listen((OrderStatus _orderStatus) {
      setState(() {
        orderStatus.add(_orderStatus);
      });
    }, onError: (a) {}, onDone: () {});
  }

  List<SimpleDialogOption> getOrderStatusDialog(BuildContext context) {
    var cancelDialog = SimpleDialogOption(
      child: Text(
        'الغاء الطلب',
        style: TextStyle(color: Colors.red, fontSize: 18),
      ),
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Wrap(
                spacing: 10,
                children: <Widget>[
                  Icon(Icons.report, color: Colors.orange),
                  Text(
                    S.of(context).confirmation,
                    style: TextStyle(color: Colors.orange),
                  ),
                ],
              ),
              content: Text(S.of(context).areYouSureYouWantToCancelThisOrder),
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 30, vertical: 25),
              actions: <Widget>[
                FlatButton(
                  child: new Text(
                    S.of(context).yes,
                    style: TextStyle(color: Theme.of(context).hintColor),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("سبب الغاء الطلب"),
                          content: TextField(
                            controller: hintText,
                            decoration: InputDecoration(
                              hintText: 'ادخل سبب الالغاء ',
                            ),
                          ),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            Row(
                              children: <Widget>[
                                new FlatButton(
                                  child: new Text("الغاء"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                new FlatButton(
                                  onPressed: () {
                                    order.active = false;
                                    order.hint = hintText.text;
                                    listenCancelOrder().then((value) {
                                      Navigator.of(context).pop();
                                    });
                                  },
                                  child: new Text("حفظ"),
                                )
                              ],
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
                FlatButton(
                  child: new Text(
                    S.of(context).cancel,
                    style: TextStyle(color: Colors.orange),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );

    List<SimpleDialogOption> _orderStatusSteps = [];
    this.orderStatus.forEach((OrderStatus _orderStatus) {
      _orderStatusSteps.add(SimpleDialogOption(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text(_orderStatus.status, style: TextStyle(fontSize: 18),),
        ),
        onPressed: () {
          order.orderStatus = _orderStatus;
          updateOrderStatus(order).then((value) {
            Navigator.of(context).pop();
            refreshOrder();
          });
        },
      ));
    });
    _orderStatusSteps.removeAt(0);
    _orderStatusSteps.add(cancelDialog);
    return _orderStatusSteps;
  }

  List<SimpleDialogOption> getDriversItems(BuildContext context) {
    List<SimpleDialogOption> _driversItem = [];
    this.drivers.forEach((User _driver) {
      _driversItem.add(SimpleDialogOption(
        child: Text(_driver.name),
        onPressed: () {
          order.driver = _driver;
          updateOrderDriver(order).then((value) {
            Navigator.of(context).pop();
            refreshOrder();
          });
        },
      ));
    });
    return _driversItem;
  }

  void listenForDrivers({String message}) async {
    final Stream<User> stream = await getDrivers();
    stream.listen((User _user) {
      setState(() {
        drivers.add(_user);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshDrivers() async {
    drivers.clear();
    listenForDrivers(message: S
        .of(context)
        .drivers);
  }

  Future<void> listenCancelOrder() async {
    cancelOrder(order).then((value) {
      Navigator.of(context).pop();
      refreshOrder();
    });
  }

}
