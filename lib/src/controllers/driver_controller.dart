import 'package:deliveryboy/src/helpers/maps_util.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/user.dart';
import '../repository/driver_repository.dart';

class DriverController extends ControllerMVC {
  List<User> drivers = <User>[];
  GlobalKey<ScaffoldState> scaffoldKey;
  MapsUtil mapsUtil = new MapsUtil();

  DriverController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void listenForDrivers({String message}) async {
    final Stream<User> stream = await getDrivers();
    stream.listen((User _user) {
      setState(() {
        drivers.add(_user);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshDrivers() async {
    drivers.clear();
    listenForDrivers(message: S.of(context).drivers);
  }

  void listenForAllDrivers({String message}) async {
    final Stream<User> stream = await getAllDrivers();
    stream.listen((User _user) {
      setState(() {
        drivers.add(_user);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshAllDrivers() async {
    drivers.clear();
    listenForAllDrivers(message: S.of(context).drivers);
  }
}
