import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../helpers/helper.dart';
import '../models/food.dart';

class AdminFoodItemWidget extends StatelessWidget {
  final String heroTag;
  final Food food;
  final Function(bool value) onTap;

  const AdminFoodItemWidget({Key key, this.food, this.heroTag, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SwitchListTile(
        title: Text(food.name ?? ' '),
        subtitle: Helper.getPrice(food.price, context,
            style: Theme.of(context).textTheme.caption),
        value: food.is_available ?? false,
        onChanged: (value) => onTap(value),
        secondary: Hero(
          tag: heroTag + food.id,
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: CachedNetworkImage(
              height: 60,
              width: 60,
              fit: BoxFit.cover,
              imageUrl: food.image.url,
              placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
                height: 60,
                width: 60,
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
      ),
    );
  }
}
