import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

import '../helpers/maps_util.dart';
import '../models/user.dart';

class DriverItemWidget extends StatefulWidget {
  final User driver;
  final LocationData restaurantLocation;

  DriverItemWidget({Key key, this.driver, this.restaurantLocation})
      : super(key: key);

  @override
  _DriverItemWidgetState createState() => _DriverItemWidgetState();
}

class _DriverItemWidgetState extends State<DriverItemWidget> {
  MapsUtil mapsUtil = new MapsUtil();
  String distance = '';

  @override
  void initState() {
    final DBRef = FirebaseDatabase.instance.reference();
    DBRef.child(widget.driver.id).once().then((snapshot) {
      print('first get for ' + widget.driver.id);
      if (snapshot.value != null)
        setState(() {
          widget.driver.driver_time = snapshot.value["time"];
          widget.driver.driver_lat = snapshot.value["lat"];
          widget.driver.driver_lng = snapshot.value["lang"];
          distance = mapsUtil
                      .calculateDistance(
                          widget.restaurantLocation.latitude,
                          widget.restaurantLocation.longitude,
                          widget.driver.driver_lat,
                          widget.driver.driver_lng)
                      .toString()
                      .length >
                  4
              ? mapsUtil
                  .calculateDistance(
              widget.restaurantLocation.latitude,
              widget.restaurantLocation.longitude,
              widget.driver.driver_lat,
              widget.driver.driver_lng)
                  .toString()
                  .substring(0, 4)
              : mapsUtil
                  .calculateDistance(
              widget.restaurantLocation.latitude,
              widget.restaurantLocation.longitude,
              widget.driver.driver_lat,
              widget.driver.driver_lng)
                  .toString();
        });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 3,),
                  Text(widget.driver.name, style: Theme
                      .of(context)
                      .textTheme
                      .subtitle1,),
                  SizedBox(height: 3,),
                  widget.driver.driver_lat != null &&
                      widget.driver.driver_lng != null
                      ? Text(' يبعد عنك مسافة ' + distance + ' كم ')
                      : Text(''),
                  SizedBox(height: 3,),
                  widget.driver.driver_time != null
                      ? Text(timeago.format(
                      DateTime.fromMillisecondsSinceEpoch(
                          widget.driver.driver_time),
                      locale: 'ar') ??
                      ' ')
                      : Text(''),
                  SizedBox(height: 3,),
                ],
              ),
            ),
            SizedBox(width: 20),
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                padding: EdgeInsets.all(0),
                iconSize: 18,
                icon: new Image.asset(
                    'assets/img/google-maps.png'),
                color: Theme
                    .of(context)
                    .accentColor,
                onPressed: () {
                  launch(
                      'https://www.google.com/maps/search/?api=1&query=${widget
                          .driver.driver_lat},${widget.driver.driver_lng}');
                },
              ),
            ),
            SizedBox(width: 10),
            SizedBox(
              width: 30,
              height: 30,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  launch("tel:${widget.driver.phone}");
                },
                child: Icon(
                  Icons.call,
                  color: Theme
                      .of(context)
                      .primaryColor,
                  size: 18,
                ),
                color: Theme
                    .of(context)
                    .accentColor
                    .withOpacity(0.9),
                shape: StadiumBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
