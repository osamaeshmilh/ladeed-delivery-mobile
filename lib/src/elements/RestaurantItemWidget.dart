import 'package:cached_network_image/cached_network_image.dart';
import 'package:deliveryboy/src/models/restaurant.dart';
import 'package:deliveryboy/src/models/route_argument.dart';
import 'package:flutter/material.dart';

class RestaurantItemWidget extends StatelessWidget {
  final String heroTag;
  final Restaurant restaurant;
  final Function(bool value) onTap;

  const RestaurantItemWidget(
      {Key key, this.restaurant, this.heroTag, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed('/RestuarantFoods',
            arguments: RouteArgument(id: restaurant.id));
      },
      child: Card(
        child: ListTile(
          title: Text(restaurant.name ?? ' '),
          subtitle: Text(restaurant.phone ?? ' '),
          trailing: Switch(
            value: !(restaurant.closed ?? true),
            onChanged: (value) => onTap(!value),
          ),
          leading: Hero(
            tag: heroTag + restaurant.id,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              child: CachedNetworkImage(
                height: 60,
                width: 60,
                fit: BoxFit.cover,
                imageUrl: restaurant.image.url,
                placeholder: (context, url) => Image.asset(
                  'assets/img/loading.gif',
                  fit: BoxFit.cover,
                  height: 60,
                  width: 60,
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
