import 'package:flutter/material.dart';

import '../models/order.dart';
import 'EmptyOrdersWidget.dart';
import 'ResturantOrderItemWidget.dart';

class OrdersListWidget extends StatefulWidget {
  final List<Order> orders;

  const OrdersListWidget({Key key, this.orders}) : super(key: key);

  @override
  _OrderListWidgetState createState() => _OrderListWidgetState();
}

class _OrderListWidgetState extends State<OrdersListWidget> {
  @override
  Widget build(BuildContext context) {
    return widget.orders.isEmpty
        ? EmptyOrdersWidget()
        : ListView.separated(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            primary: false,
            itemCount: widget.orders.length,
            itemBuilder: (context, index) {
              return ResturantOrderItemWidget(
                  expanded: index == 0 ? true : false,
                  order: widget.orders[index]);
            },
            separatorBuilder: (context, index) {
              return SizedBox(height: 20);
            },
          );
  }
}
