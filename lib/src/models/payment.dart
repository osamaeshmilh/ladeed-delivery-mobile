import '../helpers/custom_trace.dart';

class Payment {
  String id;
  String status;
  String method;

  String paymentArabic() {
    if (method == 'Pay on Pickup')
      return 'الدفع في المطعم';
    else if (method == 'Cash on Delivery')
      return 'الدفع عند التوصيل';
    else
      return ('غير محدد');
  }

  Payment.init();

  Payment(this.method);

  Payment.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      status = jsonMap['status'] ?? '';
      method = jsonMap['method'] ?? '';
    } catch (e) {
      id = '';
      status = '';
      method = '';
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'status': status,
      'method': method,
    };
  }
}
