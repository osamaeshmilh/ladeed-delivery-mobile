class Subscription {
  String id;
  String name;
  DateTime start_date;
  DateTime end_date;
  String status;

  Subscription();

  Subscription.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'] != null ? jsonMap['name'].toString() : '';
      start_date = DateTime.parse(jsonMap['start_date']) ?? new DateTime(0);
      end_date = DateTime.parse(jsonMap['end_date']) ?? new DateTime(0);
      status = jsonMap['status'] != null ? jsonMap['status'].toString() : '';
    } catch (e) {
      id = '';
      name = '';
      start_date = new DateTime(0);
      end_date = new DateTime(0);
      status = '';
    }
  }

  @override
  String toString() {
    return 'Subscription{id: $id, name: $name, start_date: $start_date, end_date: $end_date, status: $status}';
  }
}
