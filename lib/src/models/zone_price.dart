import 'package:deliveryboy/src/models/zone.dart';

import '../helpers/custom_trace.dart';

class ZonePrice {
  String id;
  double price;
  int is_available;
  String restaurant_id;
  Zone zone;

  ZonePrice();

  ZonePrice.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      price = jsonMap['price'] != null ? jsonMap['price'].toDouble() : 0.0;
      zone = jsonMap['zone'] != null
          ? Zone.fromJSON(jsonMap['zone'])
          : new Zone.init();
      is_available = jsonMap['is_available'] ?? 0;
    } catch (e) {
      id = '';
      price = 0.0;
      is_available = 0;
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }
}
