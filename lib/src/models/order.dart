import '../helpers/custom_trace.dart';
import '../models/address.dart';
import '../models/food_order.dart';
import '../models/order_status.dart';
import '../models/payment.dart';
import '../models/user.dart';

class Order {
  String id;
  List<FoodOrder> foodOrders;
  OrderStatus orderStatus;
  double tax;
  double deliveryFee;
  String hint;
  DateTime dateTime;
  User driver;
  User user;
  Payment payment;
  Address deliveryAddress;
  bool active;

  bool is_delivery;

  String isDeliveryAr() {
    return is_delivery ? 'توصيل' : 'استلام';
  }

  Order();

  Order.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      tax = jsonMap['tax'] != null ? jsonMap['tax'].toDouble() : 0.0;
      deliveryFee = jsonMap['delivery_fee'] != null
          ? jsonMap['delivery_fee'].toDouble()
          : 0.0;
      hint = jsonMap['hint'].toString();
      is_delivery = jsonMap['is_delivery'] ?? false;

      orderStatus = jsonMap['order_status'] != null
          ? OrderStatus.fromJSON(jsonMap['order_status'])
          : new OrderStatus();
      dateTime = DateTime.parse(jsonMap['updated_at']);
      user =
          jsonMap['user'] != null ? User.fromJSON(jsonMap['user']) : new User();
      driver = jsonMap['driver'] != null
          ? User.fromJSON(jsonMap['driver'])
          : new User();
      payment = jsonMap['payment'] != null
          ? Payment.fromJSON(jsonMap['payment'])
          : new Payment.init();
      deliveryAddress = jsonMap['delivery_address'] != null
          ? Address.fromJSON(jsonMap['delivery_address'])
          : new Address();
      foodOrders = jsonMap['food_orders'] != null
          ? List.from(jsonMap['food_orders'])
              .map((element) => FoodOrder.fromJSON(element))
              .toList()
          : [];
      active = jsonMap['active'] ?? true;
    } catch (e) {
      id = '';
      tax = 0.0;
      deliveryFee = 0.0;
      hint = '';
      orderStatus = new OrderStatus();
      is_delivery = false;
      dateTime = DateTime(0);
      user = new User();
      payment = new Payment.init();
      deliveryAddress = new Address();
      foodOrders = [];
      active = true;
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["user_id"] = user?.id;
    map["order_status_id"] = orderStatus?.id;
    map["tax"] = tax;
    map["delivery_fee"] = deliveryFee;
    map["is_delivery"] = is_delivery;
    map["foods"] = foodOrders.map((element) => element.toMap()).toList();
    map["payment"] = payment?.toMap();
    if (deliveryAddress?.id != null && deliveryAddress?.id != 'null')
      map["delivery_address_id"] = deliveryAddress.id;
    return map;
  }

  Map deliveredMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["order_status_id"] = 5;
    return map;
  }

  Map statusMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["order_status_id"] = orderStatus.id;
    return map;
  }

  Map cancelMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["active"] = false;
    map["hint"] = hint;
    return map;
  }

  bool canCancelOrder() {
    return this.active == true; // 1 for order received status
  }

  Map driverMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["driver_id"] = driver.id;
    return map;
  }

  @override
  String toString() {
    return 'Order{id: $id, foodOrders: $foodOrders, orderStatus: $orderStatus, tax: $tax, deliveryFee: $deliveryFee, hint: $hint, dateTime: $dateTime, driver: $driver, user: $user, payment: $payment, deliveryAddress: $deliveryAddress, is_delivery: $is_delivery}';
  }
}
