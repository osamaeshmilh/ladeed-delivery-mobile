import 'package:deliveryboy/src/controllers/profile_controller.dart';
import 'package:deliveryboy/src/elements/EmptyProfileWidget.dart';
import 'package:deliveryboy/src/elements/ShoppingRCartButtonWidget.dart';
import 'package:deliveryboy/src/models/distance_price.dart';
import 'package:deliveryboy/src/models/zone_price.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../elements/CircularLoadingWidget.dart';

class ResturantProfileWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  ResturantProfileWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _ResturantProfileWidgetState createState() => _ResturantProfileWidgetState();
}

class _ResturantProfileWidgetState extends StateMVC<ResturantProfileWidget> {
  ProfileController _con;
  TextEditingController deliveryFeeText = new TextEditingController();

  _ResturantProfileWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForUserRestaurant();
    //_con.listenForRecentOrders();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).primaryColor),
          onPressed: () => widget.parentScaffoldKey?.currentState?.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).profile,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
        actions: <Widget>[
          new ShoppingRCartButtonWidget(
              iconColor: Theme.of(context).primaryColor,
              labelColor: Theme.of(context).hintColor),
        ],
      ),
      key: _con.scaffoldKey,
      body: _con.user.apiToken == null
          ? CircularLoadingWidget(height: 500)
          : RefreshIndicator(
              onRefresh: _con.refreshRestaurantProfile,
              child: SingleChildScrollView(
//              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                child: _con.restaurant.id == null
                    ? EmptyProfileWidget()
                    : Column(
                        children: <Widget>[
                          Card(
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              trailing:
                                  _con.restaurant.subscriptionStatus == 'active'
                                      ? Container(
                                          width: 25,
                                          height: 25,
                                          decoration: BoxDecoration(
                                            color: Colors.green,
                                            shape: BoxShape.circle,
                                          ),
                                        )
                                      : Container(
                                          width: 25,
                                          height: 25,
                                          decoration: BoxDecoration(
                                            color: Colors.red,
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                              title: Text(
                                _con.restaurant.subscriptionName ?? ' ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              subtitle: _con.restaurant.subscriptionEnd != null
                                  ? Text(' تاريخ الانتهاء ' +
                                      DateFormat('yyyy/MM/dd', 'en').format(
                                          _con.restaurant.subscriptionEnd))
                                  : Text(''),
                            ),
                          ),
                          Card(
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              leading: Icon(
                                Icons.store,
                                color: Theme.of(context).hintColor,
                              ),
                              title: Text(
                                _con.restaurant.name ?? ' ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              subtitle: Text(_con.restaurant.phone ?? ' '),
                            ),
                          ),
                          Card(
                            child: SwitchListTile(
                              title: const Text('مفتوح'),
                              value: !(_con.restaurant.closed ?? true),
                              onChanged: (bool value) {
                                setState(() {
                                  _con.restaurant.closed = !value;
                                  _con.listenUpdateRestaurant();
                                });
                              },
                              secondary: const Icon(Icons.lightbulb_outline),
                            ),
                          ),
                          Card(
                            child: SwitchListTile(
                              title: const Text('الاستلام من المطعم'),
                              value:
                                  _con.restaurant.available_for_pickup ?? false,
                              onChanged: (bool value) {
                                setState(() {
                                  _con.restaurant.available_for_pickup = value;
                                  _con.listenUpdateRestaurant();
                                });
                              },
                              secondary: const Icon(Icons.shopping_basket),
                            ),
                          ),
                          Card(
                            child: SwitchListTile(
                              title: const Text('التوصيل'),
                              value:
                                  _con.restaurant.availableForDelivery ?? false,
                              onChanged: (bool value) {
                                setState(() {
                                  _con.restaurant.availableForDelivery = value;
                                  _con.listenUpdateRestaurant();
                                });
                              },
                              secondary: const Icon(Icons.directions_car),
                            ),
                          ),
                          Divider(),
                          Text(
                            'التوصيل',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          Divider(),
                          Card(
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              title: Text('مسافة التوصيل'),
                              trailing: Text(
                                _con.restaurant.deliveryRange.toString() +
                                        ' كم ' ??
                                    ' ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                            ),
                          ),
                          Card(
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              title: Text('أقل مبلغ للطلب بالتوصيل'),
                              trailing: Text(
                                _con.restaurant.minimum_order_total_for_delivery
                                            .toString() +
                                        ' دينار ' ??
                                    ' ',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                            ),
                          ),
                          Card(
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              title: Text('طريقة احتساب تكاليف التوصيل'),
                              trailing: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  _con.restaurant.deliveryPriceTypeArString() ??
                                      ' ',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      .merge(TextStyle(
                                          color:
                                              Theme.of(context).primaryColor)),
                                ),
                              ),
                            ),
                          ),
                          _con.restaurant.delivery_price_type == 'fixed'
                              ? Card(
                                  child: ListTile(
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 5),
                                    title: Text('سعر التوصيل'),
                                    trailing: Text(
                                      _con.restaurant.deliveryFee.toString() +
                                              ' دينار ' ??
                                          ' ',
                                      style:
                                          Theme.of(context).textTheme.headline4,
                                    ),
                                  ),
                                )
                              : Container(),
                          _con.restaurant.delivery_price_type == 'distance' &&
                                  _con.restaurant.deliveryFee != null
                              ? getDistancePrices(
                                  List.from(_con.restaurant.deliveryFee)
                                      .map((element) =>
                                          DistancePrice.fromJSON(element))
                                      .toSet()
                                      .toList())
                              : Container(),
                          _con.restaurant.delivery_price_type == 'zone' &&
                                  _con.restaurant.deliveryFee != null
                              ? getZonePrices(List.from(
                                      _con.restaurant.deliveryFee)
                                  .map((element) => ZonePrice.fromJSON(element))
                                  .toSet()
                                  .toList())
                              : Container(),
                        ],
                      ),
              ),
            ),
    );
  }

  Widget getDistancePrices(List<DistancePrice> distancePrices) {
    return Column(
        children: distancePrices.map((DistancePrice distancePrice) {
      return Card(
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          title: Text(distancePrice.from.toString() +
              ' - ' +
              distancePrice.to.toString() +
              ' كم '),
          trailing: Text(
            distancePrice.price.toString() + ' دينار ' ?? ' ',
            style: Theme.of(context).textTheme.headline4,
          ),
        ),
      );
    }).toList());
  }

  Widget getZonePrices(List<ZonePrice> zonePrices) {
    return Column(
        children: zonePrices.map((ZonePrice zonePrice) {
      return Card(
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          title: Text(zonePrice.zone.city.name + ' / ' + zonePrice.zone.name),
          trailing: Text(
            zonePrice.price.toString() + ' دينار ' ?? ' ',
            style: Theme.of(context).textTheme.headline4,
          ),
        ),
      );
    }).toList());
  }
}
