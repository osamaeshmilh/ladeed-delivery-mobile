import 'package:deliveryboy/src/elements/CircularLoadingWidget.dart';
import 'package:deliveryboy/src/elements/ResturantDrawerWidget.dart';
import 'package:deliveryboy/src/elements/ResturantFoodOrderItemWidget.dart';
import 'package:deliveryboy/src/elements/ShoppingRCartButtonWidget.dart';
import 'package:deliveryboy/src/helpers/maps_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

import '../../generated/l10n.dart';
import '../controllers/order_details_controller.dart';
import '../helpers/helper.dart';
import '../models/route_argument.dart';

class RestaurantOrderWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  RestaurantOrderWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _ResturantOrderWidgetState createState() {
    return _ResturantOrderWidgetState();
  }
}

class _ResturantOrderWidgetState extends StateMVC<RestaurantOrderWidget>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  int _tabIndex = 0;
  OrderDetailsController _con;

  MapsUtil mapsUtil = new MapsUtil();
  int driver_time;
  double driver_lat;
  double driver_lng;
  LocationData restaurantLocation;
  String distance = '';

  _ResturantOrderWidgetState() : super(OrderDetailsController()) {
    _con = controller;
  }

  @override
  void initState() {
//    var location = new Location();
//    location.getLocation().then((value) {
//      restaurantLocation = value;
//      setState(() {});
//
//      final DBRef = FirebaseDatabase.instance.reference();
//      DBRef.child(widget.driver.id).once().then((snapshot) {
//        print('first get for ' + widget.driver.id);
//        print(snapshot.value["time"]);
//        setState(() {
//          driver_time = snapshot.value["time"];
//          driver_lat = snapshot.value["lat"];
//          driver_lng = snapshot.value["lang"];
//          distance = mapsUtil
//              .calculateDistance(restaurantLocation.latitude,
//              restaurantLocation.longitude, driver_lat, driver_lng)
//              .toString()
//              .substring(0, 4);
//        });
//      });
//    });

    _con.listenForOrderStatus();
    _con.listenForOrder(id: widget.routeArgument.id);
    _con.listenForDrivers();
    _tabController =
        TabController(length: 3, initialIndex: _tabIndex, vsync: this);
    _tabController.addListener(_handleTabSelection);
    super.initState();
  }

  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      drawer: ResturantDrawerWidget(),
      bottomNavigationBar: _con.order == null
          ? Container(
              height: 193,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).focusColor.withOpacity(0.15),
                        offset: Offset(0, -2),
                        blurRadius: 5.0)
                  ]),
              child: SizedBox(
                width: MediaQuery.of(context).size.width - 40,
              ),
            )
          : Container(
              height: 250,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).focusColor.withOpacity(0.15),
                        offset: Offset(0, -2),
                        blurRadius: 5.0)
                  ]),
              child: SizedBox(
                width: MediaQuery.of(context).size.width - 40,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            S.of(context).subtotal,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                        Helper.getPrice(
                            Helper.getSubTotalOrdersPrice(_con.order), context,
                            style: Theme.of(context).textTheme.subtitle1)
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            S.of(context).delivery_fee,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                        Helper.getPrice(_con.order.deliveryFee, context,
                            style: Theme.of(context).textTheme.subtitle1)
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            '${S.of(context).tax} (${_con.order.tax}%)',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                        Helper.getPrice(Helper.getTaxOrder(_con.order), context,
                            style: Theme.of(context).textTheme.subtitle1)
                      ],
                    ),
                    Divider(height: 30),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            S.of(context).total,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                        ),
                        Helper.getPrice(
                            Helper.getTotalOrdersPrice(_con.order), context,
                            style: Theme
                                .of(context)
                                .textTheme
                                .headline6)
                      ],
                    ),
                    SizedBox(height: 20),
                    _con.order.active
                        ? SizedBox(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width - 40,
                      child: FlatButton(
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return SimpleDialog(
                                  title: Text(
                                      S
                                          .of(context)
                                          .select_order_status),
                                  children:
                                  _con.getOrderStatusDialog(context),
                                );
                              });
                        },
                        padding: EdgeInsets.symmetric(vertical: 14),
                        color: Theme
                            .of(context)
                            .accentColor,
                        shape: StadiumBorder(),
                        child: Text(
                          'تعديل حالة الطلب',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Theme
                                  .of(context)
                                  .primaryColor),
                        ),
                      ),
                    )
                        : SizedBox(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width - 40,
                      child: FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 14),
                        color: Theme
                            .of(context)
                            .accentColor,
                        shape: StadiumBorder(),
                        child: Text(
                          'تعديل حالة الطلب',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Theme
                                  .of(context)
                                  .primaryColor),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            ),
      body: _con.order == null
          ? CircularLoadingWidget(height: 400)
          : CustomScrollView(slivers: <Widget>[
              SliverAppBar(
                snap: true,
                floating: true,
                automaticallyImplyLeading: false,
                leading: new IconButton(
                  icon:
                      new Icon(Icons.sort, color: Theme.of(context).hintColor),
                  onPressed: () => _con.scaffoldKey?.currentState?.openDrawer(),
                ),
                centerTitle: true,
                title: Text(
                  S.of(context).order_details,
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      .merge(TextStyle(letterSpacing: 1.3)),
                ),
                actions: <Widget>[
                  new ShoppingRCartButtonWidget(
                      iconColor: Theme.of(context).hintColor,
                      labelColor: Theme.of(context).accentColor),
                ],
                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                expandedHeight: 240,
                elevation: 0,
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                    margin: EdgeInsets.only(top: 95, bottom: 65),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 16),
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor.withOpacity(0.9),
                      boxShadow: [
                        BoxShadow(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.1),
                            blurRadius: 5,
                            offset: Offset(0, 2)),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      S.of(context).order_id +
                                          ": #${_con.order.id}",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style:
                                          Theme.of(context).textTheme.headline4,
                                    ),
                                    Text(
                                      _con.order.orderStatus.status,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                    Text(
                                      DateFormat('yyyy/MM/dd - h:mm a ', 'en')
                                              .format(_con.order.dateTime) +
                                          timeago.format(_con.order.dateTime,
                                              locale: 'ar'),
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: 8),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Helper.getPrice(
                                      Helper.getTotalOrdersPrice(_con.order),
                                      context,
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .headline4),
                                  Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(100),
                                        ),
                                        color: _con.order.is_delivery
                                            ? Colors.green
                                            : Colors.orange),
                                    child: Text(
                                      '${_con.order.isDeliveryAr()}',
                                      maxLines: 1,
                                      overflow: TextOverflow.fade,
                                      softWrap: false,
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .caption
                                          .merge(TextStyle(
                                          height: 1,
                                          color: Theme
                                              .of(context)
                                              .primaryColor)),
                                    ),
                                  ),
                                  Text(
                                    S
                                        .of(context)
                                        .items +
                                        ':' +
                                        _con.order.foodOrders?.length
                                            ?.toString() ??
                                        0,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .caption,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  collapseMode: CollapseMode.pin,
                ),
                bottom: TabBar(
                    controller: _tabController,
                    indicatorSize: TabBarIndicatorSize.label,
                    labelPadding: EdgeInsets.symmetric(horizontal: 10),
                    unselectedLabelColor: Theme.of(context).accentColor,
                    labelColor: Theme.of(context).primaryColor,
                    indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Theme.of(context).accentColor),
                    tabs: [
                      Tab(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border: Border.all(
                                  color: Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.2),
                                  width: 1)),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(S
                                .of(context)
                                .foods),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border: Border.all(
                                  color: Theme
                                      .of(context)
                                      .accentColor
                                      .withOpacity(0.2),
                                  width: 1)),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(S
                                .of(context)
                                .customer),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border: Border.all(
                                  color: Theme
                                      .of(context)
                                      .accentColor
                                      .withOpacity(0.2),
                                  width: 1)),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(S
                                .of(context)
                                .drivers),
                          ),
                        ),
                      ),
                    ]),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  Offstage(
                    offstage: 0 != _tabIndex,
                    child: ListView.separated(
                      padding: EdgeInsets.only(top: 20, bottom: 50),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      primary: false,
                      itemCount: _con.order.foodOrders?.length ?? 0,
                      separatorBuilder: (context, index) {
                        return SizedBox(height: 15);
                      },
                      itemBuilder: (context, index) {
                        return ResturantFoodOrderItemWidget(
                            heroTag: 'my_orders',
                            order: _con.order,
                            foodOrder: _con.order.foodOrders.elementAt(index));
                      },
                    ),
                  ),
                  Offstage(
                    offstage: 1 != _tabIndex,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 7),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      S
                                          .of(context)
                                          .fullName,
                                      overflow: TextOverflow.ellipsis,
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .caption,
                                    ),
                                    Text(
                                      _con.order.user.name,
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .bodyText1,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: 20),
                              SizedBox(
                                width: 42,
                                height: 42,
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  disabledColor: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.4),
                                  //onPressed: () {
//                                    Navigator.of(context).pushNamed('/Profile',
//                                        arguments: new RouteArgument(param: _con.order.deliveryAddress));
                                  //},
                                  child: Icon(
                                    Icons.person,
                                    color: Theme.of(context).primaryColor,
                                    size: 24,
                                  ),
                                  color: Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.9),
                                  shape: StadiumBorder(),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 7),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      S
                                          .of(context)
                                          .deliveryAddress,
                                      overflow: TextOverflow.ellipsis,
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .caption,
                                    ),
                                    Text(
                                      _con.order.deliveryAddress?.address ??
                                          S
                                              .of(context)
                                              .address_not_provided_please_call_the_client,
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .bodyText1,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: 20),
                              SizedBox(
                                width: 42,
                                height: 42,
                                child: IconButton(
                                  padding: EdgeInsets.all(0),
                                  iconSize: 24,
                                  icon: new Image.asset(
                                      'assets/img/google-maps.png'),
                                  color: Theme.of(context).accentColor,
                                  onPressed: () {
                                    launch(
                                        'https://www.google.com/maps/search/?api=1&query=' +
                                            _con.order.deliveryAddress.latitude
                                                .toString() +
                                            ',' +
                                            _con.order.deliveryAddress.longitude
                                                .toString());
                                  },
                                ),
                              ),
                              SizedBox(width: 10),
                              SizedBox(
                                width: 42,
                                height: 42,
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  disabledColor: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.4),
                                  onPressed: () {
                                    Navigator.of(context).pushNamed('/Map',
                                        arguments: RouteArgument(
                                            id: '3', param: _con.order));
                                  },
                                  child: Icon(
                                    Icons.directions,
                                    color: Theme.of(context).primaryColor,
                                    size: 24,
                                  ),
                                  color: Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.9),
                                  shape: StadiumBorder(),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 7),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      S
                                          .of(context)
                                          .phoneNumber,
                                      overflow: TextOverflow.ellipsis,
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .caption,
                                    ),
                                    Text(
                                      _con.order.user.phone,
                                      overflow: TextOverflow.ellipsis,
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .bodyText1,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: 10),
                              SizedBox(
                                width: 42,
                                height: 42,
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  onPressed: () {
                                    launch("tel:${_con.order.user.phone}");
                                  },
                                  child: Icon(
                                    Icons.call,
                                    color: Theme.of(context).primaryColor,
                                    size: 24,
                                  ),
                                  color: Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.9),
                                  shape: StadiumBorder(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Offstage(
                    offstage: 2 != _tabIndex,
                    child: _con.order.is_delivery
                        ? Container(
                      child: _con.order.driver.id != null
                          ? Column(
                        children: <Widget>[
                          Card(
                            child: ListTile(
                              title: Text(
                                  _con.order.driver.name ?? ' '),
                              subtitle: Text(
                                  _con.order.driver.phone ?? ' '),
                              trailing: SizedBox(
                                width: 35,
                                height: 35,
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  onPressed: () {
                                    launch(
                                        "tel:${_con.order.driver.phone}");
                                  },
                                  child: Icon(
                                    Icons.call,
                                    color: Theme
                                        .of(context)
                                        .primaryColor,
                                    size: 22,
                                  ),
                                  color: Theme
                                      .of(context)
                                      .accentColor
                                      .withOpacity(0.9),
                                  shape: StadiumBorder(),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          RaisedButton(
                            child: Text('تغيير السائق'),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return SimpleDialog(
                                      title: Text(S
                                          .of(context)
                                          .change_driver),
                                      children: _con
                                          .getDriversItems(context),
                                    );
                                  });
                            },
                          ),
                        ],
                      )
                          : Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 20,
                            ),
                            Text('الرجاء تحديد سائق لهذا الطلب'),
                            SizedBox(
                              height: 10,
                            ),
                            RaisedButton(
                              child: Text('تحديد سائق'),
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return SimpleDialog(
                                        title: Text(S
                                            .of(context)
                                            .select_driver),
                                        children:
                                        _con.getDriversItems(
                                            context),
                                      );
                                    });
                              },
                            ),
                          ],
                        ),
                      ),
                    )
                        : Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Center(
                        child: Text('الزبون لم يطلب خدمة التوصيل !'),
                      ),
                    ),
                  ),
                ]),
              )
            ]),
    );
  }
}
