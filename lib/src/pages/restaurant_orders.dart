import 'package:deliveryboy/src/elements/OrdersListWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/order_controller.dart';

class RestaurantOrdersWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final int currentTab = 0;

  RestaurantOrdersWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _RestaurantOrdersWidgetState createState() => _RestaurantOrdersWidgetState();
}

class _RestaurantOrdersWidgetState extends StateMVC<RestaurantOrdersWidget> {
  OrderController _con;

  _RestaurantOrdersWidgetState() : super(OrderController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForRestaurantOrders();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: widget.currentTab ?? 0,
      length: 7,
      child: Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
            onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            S.of(context).orders,
            style: Theme.of(context)
                .textTheme
                .headline6
                .merge(TextStyle(letterSpacing: 1.3)),
          ),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.refresh, color: Theme.of(context).hintColor),
              onPressed: () => _con.refreshRestaurantOrders(),
            ),
          ],
          bottom: TabBar(
              indicatorPadding: EdgeInsets.all(10),
              labelPadding: EdgeInsets.symmetric(horizontal: 5),
              unselectedLabelColor: Theme.of(context).accentColor,
              labelColor: Theme.of(context).primaryColor,
              isScrollable: true,
              indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Theme.of(context).accentColor),
              tabs: [
                Tab(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('انتظار'),
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('الكل'),
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('جاري التجهيز'),
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('جاهز'),
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('خارج للتوصيل'),
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('تم التسليم'),
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('ملغي'),
                    ),
                  ),
                ),
              ]),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TabBarView(
            children: [
              OrdersListWidget(orders: _con.ordersWaiting),
              OrdersListWidget(orders: _con.orders),
              OrdersListWidget(orders: _con.ordersMaking),
              OrdersListWidget(orders: _con.ordersReady),
              OrdersListWidget(orders: _con.ordersOut),
              OrdersListWidget(orders: _con.ordersDelivered),
              OrdersListWidget(orders: _con.ordersCancelled),
            ],
          ),
        ),
      ),
    );
  }
}
