import 'package:deliveryboy/src/controllers/food_controller.dart';
import 'package:deliveryboy/src/elements/FoodItemWidget.dart';
import 'package:deliveryboy/src/elements/ShoppingRCartButtonWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../elements/EmptyFoodsWidget.dart';

class FoodsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  FoodsWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _FoodsWidgetState createState() => _FoodsWidgetState();
}

class _FoodsWidgetState extends StateMVC<FoodsWidget> {
  FoodController _con;

  _FoodsWidgetState() : super(FoodController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForFoods();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).foods,
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
          new ShoppingRCartButtonWidget(
              iconColor: Theme.of(context).hintColor,
              labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshFoods,
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 10),
          children: <Widget>[
            _con.foods.isEmpty
                ? EmptyFoodsWidget()
                : ListView.separated(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    primary: false,
                    itemCount: _con.foods.length,
                    itemBuilder: (context, index) {
                      var _food = _con.foods.elementAt(index);
                      return FoodItemWidget(
                          heroTag: 'details_food',
                          food: _food,
                          onTap: (value) {
                            setState(() {
                              print(_food.is_available.toString() + ' d');
                              _food.is_available = value;
                              _con.listenUpdateFood(_food);
                            });
                          });
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(height: 5);
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
