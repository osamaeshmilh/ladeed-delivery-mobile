import 'package:deliveryboy/src/controllers/food_controller.dart';
import 'package:deliveryboy/src/elements/AdminFoodItemWidget.dart';
import 'package:deliveryboy/src/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../elements/EmptyFoodsWidget.dart';

class RestuarantFoodsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final RouteArgument routeArgument;

  RestuarantFoodsWidget({Key key, this.parentScaffoldKey, this.routeArgument})
      : super(key: key);

  @override
  _RestuarantFoodsWidgetState createState() => _RestuarantFoodsWidgetState();
}

class _RestuarantFoodsWidgetState extends StateMVC<RestuarantFoodsWidget> {
  FoodController _con;

  _RestuarantFoodsWidgetState() : super(FoodController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForResturantFoods(id: widget.routeArgument.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).foods,
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshFoods,
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 10),
          children: <Widget>[
            _con.foods.isEmpty
                ? EmptyFoodsWidget()
                : ListView.separated(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    primary: false,
                    itemCount: _con.foods.length,
                    itemBuilder: (context, index) {
                      var _food = _con.foods.elementAt(index);
                      return AdminFoodItemWidget(
                          heroTag: 'details_food',
                          food: _food,
                          onTap: (value) {
                            setState(() {
                              print(_food.is_available.toString() + ' d');
                              _food.is_available = value;
                              _con.listenUpdateFoodAdmin(
                                  _food, widget.routeArgument.id);
                            });
                          });
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(height: 5);
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
