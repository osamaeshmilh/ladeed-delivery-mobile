import 'package:deliveryboy/src/elements/ShoppingRCartButtonWidget.dart';
import 'package:deliveryboy/src/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/driver_controller.dart';
import '../elements/DriverItemWidget.dart';
import '../elements/EmptyDriversWidget.dart';

class DriversWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  DriversWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _DriversWidgetState createState() => _DriversWidgetState();
}

class _DriversWidgetState extends StateMVC<DriversWidget> {
  DriverController _con;

  LocationData restaurantLocation;

  _DriversWidgetState() : super(DriverController()) {
    _con = controller;
  }

  @override
  void initState() {
    var location = new Location();
    location.getLocation().then((value) {
      restaurantLocation = value;
      _con.listenForDrivers();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).drivers,
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
          new ShoppingRCartButtonWidget(
              iconColor: Theme.of(context).hintColor,
              labelColor: Theme.of(context).accentColor),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.map),
        onPressed: () {
          Navigator.of(context).pushNamed('/DriversMap',
              arguments: RouteArgument(param: _con.drivers));
        },
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshDrivers,
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 10),
          children: <Widget>[
            _con.drivers.isEmpty
                ? EmptyDriversWidget()
                : ListView.separated(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    primary: false,
              itemCount: _con.drivers.length,
              itemBuilder: (context, index) {
                var _driver = _con.drivers.elementAt(index);
                return DriverItemWidget(
                        driver: _driver,
                        restaurantLocation: restaurantLocation,
                      );
                    },
              separatorBuilder: (context, index) {
                return SizedBox(height: 5);
              },
            ),
          ],
        ),
      ),
    );
  }
}
