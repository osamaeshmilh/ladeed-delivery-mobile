import 'dart:async';

import 'package:deliveryboy/src/models/route_argument.dart';
import 'package:deliveryboy/src/models/user.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../generated/l10n.dart';
import '../controllers/driver_controller.dart';

class DriversMapWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final RouteArgument routeArgument;

  DriversMapWidget({Key key, this.parentScaffoldKey, this.routeArgument})
      : super(key: key);

  @override
  _DriversMapWidgetState createState() => _DriversMapWidgetState();
}

class _DriversMapWidgetState extends StateMVC<DriversMapWidget> {
  DriverController _con;

  LocationData restaurantLocation;
  Completer<GoogleMapController> mapController = Completer();
  BitmapDescriptor icon;
  Set<Marker> _markers = Set<Marker>();
  List<User> drivers;
  String distance = '';

  _DriversMapWidgetState() : super(DriverController()) {
    _con = controller;
  }

  @override
  void initState() {
    drivers = widget.routeArgument?.param as List<User>;
    getIcon();

    final DBRef = FirebaseDatabase.instance.reference();
    drivers.forEach((driver) {
      DBRef.child(driver.id).once().then((snapshot) {
        print('first get for ' + driver.id);
        if (snapshot.value != null)
          setState(() {
            driver.driver_time = snapshot.value["time"];
            driver.driver_lat = snapshot.value["lat"];
            driver.driver_lng = snapshot.value["lang"];
          });
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).drivers,
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
//        actions: <Widget>[
//          new ShoppingRCartButtonWidget(
//              iconColor: Theme.of(context).hintColor,
//              labelColor: Theme.of(context).accentColor),
//        ],
      ),
      body: GoogleMap(
        myLocationEnabled: true,
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: LatLng(32.880066, 13.190435),
          zoom: 12,
        ),
        onMapCreated: (GoogleMapController controller) {
          mapController.complete(controller);
        },
        markers: drivers.map((driver) {
          if (driver.driver_lat != null && driver.driver_lng != null) {
            return Marker(
                markerId: MarkerId(driver.id),
                position: LatLng(driver.driver_lat, driver.driver_lng),
                infoWindow: InfoWindow(
                  title: driver.name,
                  snippet: timeago.format(
                      DateTime.fromMillisecondsSinceEpoch(driver.driver_time),
                      locale: 'ar'),
                ),
                icon: icon);
          } else {
            return Marker(
              markerId: MarkerId(driver.id),
              position: LatLng(33.33, 33.33),
            );
            ;
          }
        }).toSet(),
      ),
    );
  }

  void getIcon() async {
    icon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        'assets/img/driving_pin.png');
  }
}
