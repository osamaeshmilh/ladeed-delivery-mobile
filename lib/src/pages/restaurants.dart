import 'package:deliveryboy/src/controllers/restaurant_controller.dart';
import 'package:deliveryboy/src/elements/EmptyRestaurantsWidget.dart';
import 'package:deliveryboy/src/elements/RestaurantItemWidget.dart';
import 'package:deliveryboy/src/models/restaurant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class RestaurantsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  RestaurantsWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _RestaurantsWidgetState createState() => _RestaurantsWidgetState();
}

class _RestaurantsWidgetState extends StateMVC<RestaurantsWidget> {
  RestaurantController _con;

  _RestaurantsWidgetState() : super(RestaurantController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForRestaurants();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "المطاعم",
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
//        actions: <Widget>[
//          new ShoppingRCartButtonWidget(
//              iconColor: Theme.of(context).hintColor,
//              labelColor: Theme.of(context).accentColor),
//        ],
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshRestaurants,
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 10),
          children: <Widget>[
            _con.restaurants.isEmpty
                ? EmptyRestaurantsWidget()
                : ListView.separated(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    primary: false,
                    itemCount: _con.restaurants.length,
                    itemBuilder: (context, index) {
                      Restaurant _restaurant =
                          _con.restaurants.elementAt(index);
                      return RestaurantItemWidget(
                          heroTag: 'details_restaurant',
                          restaurant: _restaurant,
                          onTap: (value) {
                            setState(() {
                              _restaurant.closed = value;
                              _con.listenUpdateRestaurant(_restaurant);
                            });
                          });
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(height: 5);
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
